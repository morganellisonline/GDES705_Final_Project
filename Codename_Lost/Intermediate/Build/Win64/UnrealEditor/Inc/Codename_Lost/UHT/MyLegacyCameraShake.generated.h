// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "MyLegacyCameraShake.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODENAME_LOST_MyLegacyCameraShake_generated_h
#error "MyLegacyCameraShake.generated.h already included, missing '#pragma once' in MyLegacyCameraShake.h"
#endif
#define CODENAME_LOST_MyLegacyCameraShake_generated_h

#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_SPARSE_DATA
#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_RPC_WRAPPERS
#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_ACCESSORS
#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyLegacyCameraShake(); \
	friend struct Z_Construct_UClass_UMyLegacyCameraShake_Statics; \
public: \
	DECLARE_CLASS(UMyLegacyCameraShake, ULegacyCameraShake, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Codename_Lost"), NO_API) \
	DECLARE_SERIALIZER(UMyLegacyCameraShake)


#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMyLegacyCameraShake(); \
	friend struct Z_Construct_UClass_UMyLegacyCameraShake_Statics; \
public: \
	DECLARE_CLASS(UMyLegacyCameraShake, ULegacyCameraShake, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Codename_Lost"), NO_API) \
	DECLARE_SERIALIZER(UMyLegacyCameraShake)


#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyLegacyCameraShake(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyLegacyCameraShake) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyLegacyCameraShake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyLegacyCameraShake); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyLegacyCameraShake(UMyLegacyCameraShake&&); \
	NO_API UMyLegacyCameraShake(const UMyLegacyCameraShake&); \
public: \
	NO_API virtual ~UMyLegacyCameraShake();


#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyLegacyCameraShake(UMyLegacyCameraShake&&); \
	NO_API UMyLegacyCameraShake(const UMyLegacyCameraShake&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyLegacyCameraShake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyLegacyCameraShake); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMyLegacyCameraShake) \
	NO_API virtual ~UMyLegacyCameraShake();


#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_12_PROLOG
#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_SPARSE_DATA \
	FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_RPC_WRAPPERS \
	FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_ACCESSORS \
	FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_INCLASS \
	FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_SPARSE_DATA \
	FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_ACCESSORS \
	FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_INCLASS_NO_PURE_DECLS \
	FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CODENAME_LOST_API UClass* StaticClass<class UMyLegacyCameraShake>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Github_GDES705_Final_Project_Codename_Lost_Source_Codename_Lost_MyLegacyCameraShake_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
